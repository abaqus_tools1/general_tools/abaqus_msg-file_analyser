""" created by Nathan Gerhardt
    RTLC Scania
    nathan_gerhardt@scania.com
    0722386592
"""

import plotext as plt
import plotille
import pathlib
import numpy as np
import os
import sys
import inquirer

from msgFilePlotter.lib.general.yaml_loader import yaml_load
from msgFilePlotter.lib.general.app_tools import get_file_list

# Get location of this file relative to custom library
__this_file__ = pathlib.Path(__file__)
__this_path__ = __this_file__.parent.resolve()

# Default config path
default_config_file = __this_path__.joinpath('config.yaml')

# Load configurations
config = yaml_load(default_config_file)

# Version Release
VERSION = 1.0


def main():

    msg_files_in_path = get_file_list(search_term="*.msg")

    if not msg_files_in_path:
        print('Error - No msg files exist in our current directory, exiting....')
        sys.exit()

    pivot_table_user = [
        inquirer.List('msg_file',
                      message='Which msg file data do you want to graph?',
                      choices=msg_files_in_path)
    ]

    msg_file_json = inquirer.prompt(pivot_table_user)

    print(msg_file_json["msg_file"])
    print(pathlib.Path(os.getcwd()))
    file_full_path = pathlib.Path(os.getcwd()).joinpath(msg_file_json["msg_file"])

    with open(file_full_path, 'r', errors="ignore") as f:
        step_count=0
        incr_count=0
        iter_count=0
        iter_per_incr=[]
        res_force_list=[]
        res_moment_list=[]
        x_axis=[]

        for num, line in enumerate(f, 1):
            print(line)

            #reading each line and choosing the action relative to the content
            if "                        S T E P      " in line:
                step_count_string=[str(x) for x in line.split()]
                step_count=float(step_count_string[4])
                print()
                print()
                print("Step # " + str(step_count))
                print()
                print()
                incr_count=0
            if "CRITERION FOR RESIDUAL FORCE     FOR A NONLINEAR PROBLEM" in line:
                print("CRITERION FOR RESIDUAL FORCE FOR A NONLINEAR PROBLEM")
                cr_res_force_string=[str(x) for x in line.split()]
                cr_res_force=float(cr_res_force_string[-1])
                print(cr_res_force)
            elif "CRITERION FOR RESIDUAL MOMENT    FOR A NONLINEAR PROBLEM" in line:
                print("CRITERION FOR RESIDUAL MOMENT FOR A NONLINEAR PROBLEM")
                cr_res_moment_string=[str(x) for x in line.split()]
                cr_res_moment=float(cr_res_moment_string[-1])
                print(cr_res_moment)
                # Initial setup end, defined the analysis convergence criteria
            if "  INCREMENT    " in line and " ATTEMPT NUMBER  1" in line:
                incr_count=incr_count+1
                #if it's not the first increment (incr_count>1) print the total iters for previous step, and append it in the iter_per_increment
                if incr_count >1:
                    iter_per_incr.append(iter_count)
                iter_count=0
                print("___________________")
                print("Increment  :" + str(incr_count))
            if "ITERATION  " in line and "DISCONTINUITY" in line:
                iteration_line=[str(x) for x in line.split()]
                print()
                print("INCREMENT: "+ str(incr_count) + " - Iteration : " +iteration_line[-1])
                iter_count=iter_count+1
            if "ITERATION  " in line and "EQUILIBRIUM" in line:
                iteration_line=[str(x) for x in line.split()]
                print()
                print("INCREMENT: "+ str(incr_count) + " - EQ Iteration : " +iteration_line[-1])
                iter_count=iter_count+1
            elif " AVERAGE FORCE                       " in line:
                avg_force_string=[str(x) for x in line.split()]
                avg_force=float(avg_force_string[-1])
                #print(avg_force)
            elif " LARGEST RESIDUAL FORCE        " in line:
                convergence_res_force_string=[str(x) for x in line.split()]
                #    print(convergence_res_force_string)
                convergence_res_force=float(convergence_res_force_string[3])
                residual_ratio_f=abs((convergence_res_force/avg_force))
                res_force_list.append(residual_ratio_f)
                #if abs(residual_ratio_f) > cr_res_force:
                #    print(residual_ratio_f)
            #    else:
            #    print("Step has converged for force, residual ratio= " + str(residual_ratio_f))
            elif "AVERAGE MOMENT                     " in line:
                avg_moment_string=[str(x) for x in line.split()]
                avg_moment=float(avg_moment_string[-1])
                #print(avg_moment)
            elif " LARGEST RESIDUAL MOMENT        " in line:
                convergence_res_moment_string=[str(x) for x in line.split()]
                convergence_res_moment=float(convergence_res_moment_string[3])
                residual_ratio_m=(convergence_res_moment/avg_moment)
                res_moment_list.append(residual_ratio_m)
                if abs(residual_ratio_m) < cr_res_moment and abs(residual_ratio_f) < cr_res_force:
                    print("Increment " + str(incr_count) +  " has converged")
                    print("_______________________________")
                elif abs(residual_ratio_f) > cr_res_force and abs(residual_ratio_m) > cr_res_moment:
                    print("Force has not converged, residual force ratio= " + str(residual_ratio_f))
                    print("Moment has not converged, residual moment ratio= " + str(residual_ratio_m))
                elif abs(residual_ratio_f) > cr_res_force:
                    print("Force has not converged, residual force ratio= " + str(residual_ratio_f))
                elif abs(residual_ratio_m) > cr_res_moment:
                    print("Moment has not converged, residual moment ratio= " + str(residual_ratio_m))
            elif "***NOTE" in line:
                print()
                print("Total iterations in failed increment " + str(incr_count) + " = " + str(iter_count))
                iter_count=0

        x_value=range(0, len(res_force_list))



    x = np.sort(np.random.normal(size=1000))

    fig = plotille.Figure()
    fig.width = 60
    fig.height = 30
    fig.set_x_limits(min_=-3, max_=3)
    fig.set_y_limits(min_=-1, max_=1)
    fig.color_mode = 'byte'
    fig.plot([-0.5, 1], [-1, 1], lc=25, label='First line')
    fig.scatter(x, np.sin(x), lc=100, label='sin')
    fig.plot(x, (x+2)**2 , lc=200, label='square')
    print(fig.show(legend=True))

    # logarithmic graph

    fig2 = plotille.Figure()
    fig2.width = 60
    fig2.height = 30
    fig2.color_mode = "byte"
    fig2.plot(x_value, res_force_list, label="First Line")
    print(fig2.show(legend=True))


    ###########
    l = 10 ** 4
    y = plt.sin(periods = 2, length = l)
    plt.plot(y)
    plt.plotsize(60, 30)
    plt.colors()

    plt.xscale("log")
    plt.yscale("linear")
    plt.grid(0, 1)

    plt.title("Logarithmic Plot")
    plt.xlabel("logarithmic scale")
    plt.ylabel("linear scale")

    plt.show()


if __name__ == '__main__':
    main()
