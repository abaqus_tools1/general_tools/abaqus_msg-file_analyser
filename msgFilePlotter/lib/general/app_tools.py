import os
from pathlib import Path
import subprocess


def get_file_list(path=None, search_term="*.*"):
    # Returns a list of Input Files, default is: pwd
    if not path:
        path = Path.cwd()

    files = sorted(path.glob(search_term), key=os.path.getmtime, reverse=True)

    files = [x.name for x in files if x.is_file()]

    return files
