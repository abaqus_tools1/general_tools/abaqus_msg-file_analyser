import yaml
import pathlib


def yaml_load(yaml_file):
    yaml_file = pathlib.Path(yaml_file)

    if not yaml_file.is_file():
        OSError(f'The provided file was not found: {yaml_file.absolute().as_posix()}')

    try:
        with open(yaml_file, 'r', encoding='utf-8') as config_file_handle:
            config = yaml.load(config_file_handle, Loader=yaml.SafeLoader)

    except ImportError as e:
        print(f"Could not import the config.yaml file, an error occurred in the format of {e}")

    return config
