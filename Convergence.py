import glob
import matplotlib.pyplot
import matplotlib.pyplot as plt

path=(".")
g=glob.glob('*.msg')
n=1
for f in g:
    print(str(n) +" " +f)
    n=n+1
n=int(input("which file do you want to open? 1 to " + str(len(g)) + " :"))
msg_file=open(g[n-1],"r")
lines=msg_file.readlines()
step_count=0
incr_count=0
iter_count=0
iter_per_incr=[]
res_force_list=[]
res_moment_list=[]
x_axis=[]
for line in lines:
    #reading each line and choosing the action relative to the content
    if "                        S T E P      " in line:
        step_count_string=[str(x) for x in line.split()]
        step_count=float(step_count_string[4])
        print()
        print()
        print("Step # " + str(step_count))
        print()
        print()
        incr_count=0
    if "CRITERION FOR RESIDUAL FORCE     FOR A NONLINEAR PROBLEM" in line:
        print("CRITERION FOR RESIDUAL FORCE FOR A NONLINEAR PROBLEM")
        cr_res_force_string=[str(x) for x in line.split()]
        cr_res_force=float(cr_res_force_string[-1])
        print(cr_res_force)
    elif "CRITERION FOR RESIDUAL MOMENT    FOR A NONLINEAR PROBLEM" in line:
        print("CRITERION FOR RESIDUAL MOMENT FOR A NONLINEAR PROBLEM")
        cr_res_moment_string=[str(x) for x in line.split()]
        cr_res_moment=float(cr_res_moment_string[-1])
        print(cr_res_moment)
        # Initial setup end, defined the analysis convergence criteria
    if "  INCREMENT    " in line and " ATTEMPT NUMBER  1" in line:
        incr_count=incr_count+1
        #if it's not the first increment (incr_count>1) print the total iters for previous step, and append it in the iter_per_increment
        if incr_count >1:
            iter_per_incr.append(iter_count)
        iter_count=0
        print("___________________")
        print("Increment  :" + str(incr_count))
    if "ITERATION  " in line and "DISCONTINUITY" in line:
        iteration_line=[str(x) for x in line.split()]
        print()
        print("INCREMENT: "+ str(incr_count) + " - Iteration : " +iteration_line[-1])
        iter_count=iter_count+1
    if "ITERATION  " in line and "EQUILIBRIUM" in line:
        iteration_line=[str(x) for x in line.split()]
        print()
        print("INCREMENT: "+ str(incr_count) + " - EQ Iteration : " +iteration_line[-1])
        iter_count=iter_count+1
    elif " AVERAGE FORCE                       " in line:
        avg_force_string=[str(x) for x in line.split()]
        avg_force=float(avg_force_string[-1])
        #print(avg_force)
    elif " LARGEST RESIDUAL FORCE        " in line:
        convergence_res_force_string=[str(x) for x in line.split()]
    #    print(convergence_res_force_string)
        convergence_res_force=float(convergence_res_force_string[3])
        residual_ratio_f=abs((convergence_res_force/avg_force))
        res_force_list.append(residual_ratio_f)
        #if abs(residual_ratio_f) > cr_res_force:
        #    print(residual_ratio_f)
    #    else:
        #    print("Step has converged for force, residual ratio= " + str(residual_ratio_f))
    elif "AVERAGE MOMENT                     " in line:
            avg_moment_string=[str(x) for x in line.split()]
            avg_moment=float(avg_moment_string[-1])
            #print(avg_moment)
    elif " LARGEST RESIDUAL MOMENT        " in line:
        convergence_res_moment_string=[str(x) for x in line.split()]
        convergence_res_moment=float(convergence_res_moment_string[3])
        residual_ratio_m=(convergence_res_moment/avg_moment)
        res_moment_list.append(residual_ratio_m)
        if abs(residual_ratio_m) < cr_res_moment and abs(residual_ratio_f) < cr_res_force:
            print("Increment " + str(incr_count) +  " has converged")
            print("_______________________________")
        elif abs(residual_ratio_f) > cr_res_force and abs(residual_ratio_m) > cr_res_moment:
            print("Force has not converged, residual force ratio= " + str(residual_ratio_f))
            print("Moment has not converged, residual moment ratio= " + str(residual_ratio_m))
        elif abs(residual_ratio_f) > cr_res_force:
            print("Force has not converged, residual force ratio= " + str(residual_ratio_f))
        elif abs(residual_ratio_m) > cr_res_moment:
            print("Moment has not converged, residual moment ratio= " + str(residual_ratio_m))
    elif "***NOTE" in line:
        print()
        print("Total iterations in failed increment " + str(incr_count) + " = " + str(iter_count))
        iter_count=0
x_value=range(0, len(res_force_list))
#print(res_force_list)
#print(x_value)
plt.plot(x_value, res_force_list)
matplotlib.pyplot.yscale("log")
plt.axhline(y=cr_res_force, c='red')
plt.show()
