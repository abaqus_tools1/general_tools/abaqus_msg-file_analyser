import setuptools

with open('README.md', "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='simauto_app_abaqus_convergence_plotter',
    version='0.9.2',
    author="Nathan Gerhardt",
    author_email="nathan.gerhardt@scania.com",
    description=" A small app designed to read and plot convergence in the terminal",
    install_requires=['jinja2',
                      'pyyaml',
                      'numpy',
                      'plotext',
                      'plotille'
                      ],
    dependency_links=['https://af.scania.com/artifactory/api/pypi/simautomation-pypi-local/simple'],
    license='GPLv3',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.scania.com/simautomation/autoprocess/modules/simauto_module_abaqus_modeldb",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'abaqus_msg-analyser=msgFilePlotter.convergence:main',
        ],
    },
    include_package_data=True,
    python_requires='>=3.6.8',
)
